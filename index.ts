import fsPromise from 'fs/promises';

// Інтерфейс об'єкта новин
interface NewsPost {
    id: number;
    title: string;
    text: string;
    createDate: Date;
}

// Інтерфейс для доступу до бази даних
interface Database {
    registerSchema(tableName: string, schema: any): Promise<void>;
    getTable(tableName: string): NewsTable;
}

// Реалізація бази даних
class FileDB implements Database {
    async registerSchema(tableName: string, schema: any): Promise<void> {
        try {
            const filePath = `${tableName}.json`;
            const data = {
                table: [] as NewsPost[],
                id: 1,
                schema: schema
            };
            await fsPromise.writeFile(filePath, JSON.stringify(data));
            console.log(`File ${filePath} created`);
        } catch (err) {
            console.error(err);
            throw err;
        }
    }

    getTable(tableName: string): NewsTable {
        return new NewsTable(tableName, this);
    }
}

// Клас для операцій з таблицею новин
class NewsTable {
    private readonly filePath: string;
    private readonly database: Database;

    constructor(tableName: string, database: Database) {
        this.filePath = `${tableName}.json`;
        this.database = database;
    }

    // Зчитування таблиці з файлу
    private async readTable(): Promise<{ table: NewsPost[]; id: number; schema: any }> {
        try {
            const data = await fsPromise.readFile(this.filePath, 'utf-8');
            return JSON.parse(data);
        } catch (err) {
            console.error(err);
            throw err;
        }
    }

    // Додавання нового запису
    async create(data: Partial<NewsPost>): Promise<NewsPost> {
        try {
            let { id, schema, table } = await this.readTable();
            const newId = id + 1;
            const newPost: NewsPost = {
                id: newId,
                title: data.title || '',
                text: data.text || '',
                createDate: new Date()
            };

            for (const key in schema) {
                if (!(key in newPost)) {
                    throw new Error(`Missing property '${key}' in the provided data`);
                }
            }

            table.push(newPost);

            await fsPromise.writeFile(this.filePath, JSON.stringify({ table, id: newId, schema }));
            return newPost;
        } catch (err) {
            console.error(err);
            throw err;
        }       
    }

    // Оновлення запису
    async update(id: number, data: Partial<NewsPost>): Promise<NewsPost> {
        try {
            let { schema, table } = await this.readTable();

            // Знайти запис за заданим ідентифікатором
            const postIndex = table.findIndex(post => post.id === id);

            // Якщо запис не знайдено, викинути помилку
            if (postIndex === -1) {
                throw new Error(`Record with id ${id} not found`);
            }

            // Оновити дані запису з новими даними
            const updatedPost = { ...table[postIndex], ...data };
            table[postIndex] = updatedPost;

            // Зберегти оновлені дані у файл
            await fsPromise.writeFile(this.filePath, JSON.stringify({ table, id, schema }));
            return updatedPost;

        } catch (err) {
            console.error(err);
            throw err;
        }
    }

    // Отримання всіх записів
    async getAll(): Promise<NewsPost[]> {
        try {
            const { table } = await this.readTable();
            return table;
        } catch (err) {
            console.error(err);
            throw err;
        }
    }

    // Отримання запису за id
    async getById(id: number): Promise<NewsPost | null> {
        try {
            const { table } = await this.readTable();
            return table.find(post => post.id === id) || null;
        } catch (err) {
            console.error(err);
            throw err;
        }
    }

    // Видалення запису за id
    async delete(id: number): Promise<number> {
        try {
            let { schema, table } = await this.readTable();
            const postIndex = table.findIndex(post => post.id === id);

            if (postIndex === -1) {
                throw new Error(`Record with id ${id} not found`);
            }

            const deletedId = table.splice(postIndex, 1)[0].id;
            await fsPromise.writeFile(this.filePath, JSON.stringify({ table, id, schema }));
            return deletedId;
        } catch (err) {
            console.error(err);
            throw err;
        }
    }
}

// Приклад використання
const fileDB = new FileDB();

const newspostSchema = {
    id: Number,
    title: String,
    text: String,
    createDate: Date,
};

fileDB.registerSchema('newspost', newspostSchema);

const newspostTable = fileDB.getTable('newspost');

const data = {
    title: 'У зоопарку Чернігова лисичка народила лисеня',
    text: "В Чернігівському зоопарку сталася чудова подія! Лисичка на ім'я Руда народила чудове лисенятко! Тож поспішайте навідатись та подивитись на це миле створіння!"
};

const createdNewspost = newspostTable.create(data);
